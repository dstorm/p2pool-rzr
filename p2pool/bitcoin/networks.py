import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(

    razor=math.Object(
        P2P_PREFIX='f9e8b4d9'.decode('hex'),
        P2P_PORT=7272,
        ADDRESS_VERSION=60,
        RPC_PORT=9393,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'razoraddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: 100*100000000 >> (height + 1)//10000,
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('ltc_scrypt').getPoWHash(data)),
        BLOCK_PERIOD=75, # s
        SYMBOL='RZR',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Razor') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Razor/') if platform.system() == 'Darwin' else os.path.expanduser('~/.razor'), 'razor.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//1000000000 - 1, 2**256//1000 - 1),
        DUMB_SCRYPT_DIFF=2**16,
        DUST_THRESHOLD=0.03e8,
    ),

    razor_testnet=math.Object(
        P2P_PREFIX='ffc5c1e1'.decode('hex'),
        P2P_PORT=17272,
        ADDRESS_VERSION=121,
        RPC_PORT=19393,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'razoraddress' in (yield bitcoind.rpc_help()) and
            (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: 100*100000000 >> (height + 1)//10000,
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('ltc_scrypt').getPoWHash(data)),
        BLOCK_PERIOD=75, # s
        SYMBOL='RZR',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Razor') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Razor/') if platform.system() == 'Darwin' else os.path.expanduser('~/.razor'), 'razor.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//1000000000 - 1, 2**256//1000 - 1),
        DUMB_SCRYPT_DIFF=2**16,
        DUST_THRESHOLD=0.03e8,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
