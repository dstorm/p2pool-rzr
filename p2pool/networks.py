from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    razor=math.Object(
        PARENT=networks.nets['razor'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=15*60*60//15, # shares
        REAL_CHAIN_LENGTH=15*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=24, # blocks
        IDENTIFIER='f863584d49a0999b'.decode('hex'),
        PREFIX='723280da1ae6827d'.decode('hex'),
        P2P_PORT=7172,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=7171,
        BOOTSTRAP_ADDRS='rzr.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-rzr',
        VERSION_CHECK=lambda v: True,
    ),

    razor_testnet=math.Object(
        PARENT=networks.nets['razor_testnet'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=15*60*60//15, # shares
        REAL_CHAIN_LENGTH=15*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=24, # blocks
        IDENTIFIER='e78c1857ceb13b08'.decode('hex'),
        PREFIX='3d3eee91c40326b5'.decode('hex'),
        P2P_PORT=17172,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=17171,
        BOOTSTRAP_ADDRS=''.split(' '),
        ANNOUNCE_CHANNEL='',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
